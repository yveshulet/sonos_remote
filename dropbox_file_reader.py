
import dropbox
import datetime
import tempfile
import dropboxconfig
from enum import Enum

# ============================================
#				DEPRECATED
# ============================================



class DropboxType(Enum):
	HOME = 1
	AWAY = 2


def dropboxFileHasChanged(dropboxType):
	filepath = ''
	if(dropboxType is DropboxType.AWAY):
		filepath = dropboxconfig.AWAY_DROPBOX_FILEPATH
	else:
		filepath = dropboxconfig.HOME_DROPBOX_FILEPATH

	referenceDatetime = validateTimestampFile()
	print('reference datetime: '+str(referenceDatetime))

	dbx = dropbox.Dropbox(dropboxconfig.dbx_access_token)
	dbxAwayFile = dbx.files_get_metadata(filepath)

	return fileModified(dbxAwayFile, referenceDatetime)



def validateTimestampFile():
	timeStringRead = readTimeFromFile()
	timeRead = datetime.datetime.strptime(timeStringRead, '%Y-%m-%d %H:%M:%S.%f')
	if(type(timeRead) is not datetime.datetime):
		print("timestamp file content is not of type datetime! Current time will be written")
		writeCurrentTimeToFile()
	return timeRead


def fileModified(fileToBeChecked, referenceDatetime):
	serverDatetime = fileToBeChecked.server_modified
	print('server datetime: '+str(serverDatetime))
	return serverDatetime > referenceDatetime


def writeCurrentTimeToFile():
	timeRead = datetime.datetime.utcnow()
	writeTimeToFile(timeRead)


def writeTimeToFile(datetime):
	with open(dropboxconfig.TIME_FILENAME, 'w') as file:
		timestring = str(datetime)
		file.write(timestring)

def readTimeFromFile():
	with open(dropboxconfig.TIME_FILENAME, 'r') as file:
		return file.read()

def main():
	referenceDatetime = validateTimestampFile()
	print('reference datetime: '+str(referenceDatetime))

	dbx = dropbox.Dropbox(dropboxconfig.dbx_access_token)
	dbxAwayFile = dbx.files_get_metadata(dropboxconfig.AWAY_DROPBOX_FILEPATH)

	print('away file modified: '+fileModified(dbxAwayFile, referenceDatetime))


if __name__ == '__main__':
    main()