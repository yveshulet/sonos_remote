import nest
import nestconfig
from enum import Enum


class NestState(Enum):
	HOME = 1
	AWAY = 2



def main():
	print('nest status reader started')
	currentState = getAndValidateStateFile()
	print('nest cached state: '+currentState.name)
	remoteState = getRemoteState()
	print('nest remote state: '+remoteState.name)



def authenticate():
	napi = nest.Nest(client_id=nestconfig.client_id, client_secret=nestconfig.client_secret, access_token_cache_file=nestconfig.access_token_cache_file)
	if napi.authorization_required:
		print('Go to ' + napi.authorize_url + ' to authorize, then enter PIN below')
		pin = input("PIN: ")
		napi.request_token(pin)
	return napi


def getStateBy(napi):
	for structure in napi.structures:
	    print ('Structure %s' % structure.name)
	    print ('    Away: %s' % structure.away)
	    # leave by the first structure found (there shouldn't be more than one)
	    if structure.away == 'home':
	    	return NestState.HOME
	    else:
    		return NestState.AWAY


def getRemoteState():
	napi = authenticate()
	return getStateBy(napi)


def getAndValidateStateFile():
	stateStringRead = readStateFromFile()
	state = NestState.AWAY
	try:
		stateInt = int(stateStringRead)
		state = NestState(stateInt)
	except ValueError as e:
		print("state file content is not of type integer! Default 'AWAY' will be written")
		writeStateToFile(state)
	
	return state


def writeStateToFile(state):
	with open(nestconfig.STATE_FILENAME, 'w') as file:
		file.write(str(state.value))

def readStateFromFile():
	with open(nestconfig.STATE_FILENAME, 'r') as file:
		return file.read()


if __name__ == '__main__':
    main()
