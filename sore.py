#!/usr/bin/env python

import soco
import argparse

def play_all():
    speakers = soco.discover()
    speaker = speakers.pop()
    speaker.partymode()
    speaker.play()
    print('play all command executed')

def pause_all():
    #device = soco.discovery.any_soco()
    #device.all_groups.pause()
    #for group in device.all_groups:
    #    group.coordinator.pause()
    speakers = soco.discover()
    for speaker in speakers:
        speaker.group.coordinator.pause()

    print('pause all command executed')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("command")
    args = parser.parse_args()
    if args.command == "play":
        play_all()
    elif args.command == "pause":
        pause_all()
    else:		
        print("Command unknown. Command can either be 'play' or 'pause'")
