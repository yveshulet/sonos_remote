#!/usr/bin/env python

import sore
#import dropbox_file_reader
import nest_status_reader



def main():

	localState = nest_status_reader.getAndValidateStateFile()
	remoteState = nest_status_reader.getRemoteState()
	statusHasChanged = localState.value != remoteState.value
	
	if statusHasChanged:
		updateSonos(remoteState)
		nest_status_reader.writeStateToFile(remoteState)
		print('status has changed')
	else:
		print("status hasn't changed")


def updateSonos(nestState):
	if(nestState is nest_status_reader.NestState.HOME):
		sore.play_all()
	else:
		sore.pause_all()

if __name__ == '__main__':
    main()